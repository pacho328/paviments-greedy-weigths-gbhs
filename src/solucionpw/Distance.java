/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solucionpw;

/**
 *
 * @author Pacho328
 */
public class Distance {
    public int grupo;
    public double distance;

    public Distance(int grupo, double distance) {
        this.grupo = grupo;
        this.distance = distance;
    }

    public int getGrupo() {
        return grupo;
    }

    public double getDistance() {
        return distance;
    }
    
}
