package solucionpw;


import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.SMO;
import weka.core.converters.ConverterUtils.DataSink;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;
import weka.filters.unsupervised.attribute.Remove;

public class BestGroup {
	public List<WekaDataSet> AcceptedGroups = new ArrayList<WekaDataSet>();
	
	public void addWDS(WekaDataSet theWekaDatasSet) throws Exception{
		AcceptedGroups.add(theWekaDatasSet);
	}
	
	public void DefineGroup(Instance theInstance){
		int bestGroup = 0;
		double bestCC = 0.0;
		for (int i = 0; i < AcceptedGroups.size(); i++){

			double value = (AcceptedGroups.get(i)).EvaluateIncludeInstance(theInstance);
			if (i ==0) {
				bestGroup = i;
				bestCC = value;
			}
			if (value > bestCC){
				bestGroup = i;
				bestCC = value;				
			}
		}
		(AcceptedGroups.get(bestGroup)).IncludeInstance(theInstance);
	}

	public String TablaResultados;
	
	public double[] Evaluate(Instances TestInstances,int kn,double[] weightsHarmony) throws Exception{
        System.out.println("Evaluado el DataTest");
            
		int numInstances = TestInstances.numInstances();
		int Inside15 = 0;
		double MSE = 0.0;
		double MAE = 0.0;
		double psimax = Double.MIN_VALUE;
		double psimin = Double.MAX_VALUE;
        DecimalFormat df6 = new DecimalFormat("###.######");
		TablaResultados = "";
		for (int ins = 0; ins < numInstances; ins++){
			Instance theInstance = TestInstances.get(ins);
			
			int bestGroup = 0;
			ArrayList<Distance> ListDistanceToweigh = new ArrayList<Distance>();

			for (int i = 0; i < AcceptedGroups.size(); i++){
				ArrayList<Distance> TmpListDist =  AcceptedGroups.get(i).KlistMinDistanceToTestInstance(theInstance,kn,i,weightsHarmony);
				for (int j = 0; j < TmpListDist.size(); j++) {
					ListDistanceToweigh.add(TmpListDist.get(j));
				}
			}
			Distance FinD  = FinalDistance(ListDistanceToweigh,kn,AcceptedGroups.size());
			bestGroup = FinD.grupo;
			
			double psi = theInstance.value(TestInstances.numAttributes()-1);
			if (psi > psimax){psimax = psi;}
			if (psi < psimin){psimin = psi;}
			double predicted_psi = (AcceptedGroups.get(bestGroup)).PredictPSI(theInstance);
			TablaResultados += df6.format(psi) + "|" + df6.format(predicted_psi) + "\n";
			if (predicted_psi > psi*0.85 && predicted_psi < psi*1.15){
				Inside15++;
			}
			MSE += Math.pow(psi-predicted_psi, 2);
			MAE += Math.abs(psi-predicted_psi);
		}
		double inside = (100.0*Inside15)/numInstances;
		MSE = MSE / numInstances;
		MAE = MAE / numInstances;
		double RMSE = Math.sqrt(MSE);
		double NRMSE = RMSE/ (psimax-psimin);
		double[] result = new double[5];
		result[0] = inside;
		result[1] = MSE;
		result[2] = MAE;
		result[3] = RMSE;
		result[4] = NRMSE;
        System.out.println("15: "+inside+" | "+"MSE: "+MSE+" | "+"MAE: "+MAE+" | "+"RMSE: "+RMSE+" | "+"NRMSE: "+NRMSE+" | ");

		return result;
	}
    public double[] Evaluate(int kn,double[] weightsHarmony) throws Exception{
		Instances allgroups = joinByGroups();
		int Inside15 = 0;
		double MSE = 0.0;
		double MAE = 0.0;
		double psimax = Double.MIN_VALUE;
		double psimin = Double.MAX_VALUE;
		DecimalFormat df6 = new DecimalFormat("###.######");
		TablaResultados = "";
		int numInstances = 0;          

		for (int insG = 0; insG < AcceptedGroups.size(); insG++){
			for (int ins = 0; ins < AcceptedGroups.get(insG).DataSet.numInstances(); ins++) {
				numInstances++;
				Instance theInstance = AcceptedGroups.get(insG).DataSet.get(ins);
			
				int bestGroup = 0;
                ArrayList<Distance> ListDistanceToweigh = new ArrayList<Distance>();

				for (int i = 0; i < AcceptedGroups.size(); i++){
						for (int j = 0; j < AcceptedGroups.get(i).DataSet.numInstances(); j++) {
							if(insG == i && insG == j) continue;
							Instance theInstance2 = AcceptedGroups.get(i).DataSet.get(j);
							double dis = AcceptedGroups.get(i).distanceWeights(theInstance,theInstance2,weightsHarmony);
							Distance DisTo = new Distance(i,dis);
							ListDistanceToweigh.add(DisTo);
						}
				}

				Distance FinD  = FinalDistance(ListDistanceToweigh,kn,AcceptedGroups.size());
				bestGroup = FinD.grupo;
			
				double psi = theInstance.value(theInstance.numAttributes()-1);
				if (psi > psimax){psimax = psi;}
				if (psi < psimin){psimin = psi;}
				double predicted_psi = (AcceptedGroups.get(bestGroup)).PredictPSI(theInstance);
				TablaResultados += df6.format(psi) + "|" + df6.format(predicted_psi) + "\n";
				if (predicted_psi > psi*0.85 && predicted_psi < psi*1.15){
					Inside15++;
				}
				MSE += Math.pow(psi-predicted_psi, 2);
				MAE += Math.abs(psi-predicted_psi);
            }
		}
		double inside = (100.0*Inside15)/numInstances;
		MSE = MSE / numInstances;
		MAE = MAE / numInstances;
		double RMSE = Math.sqrt(MSE);
		double NRMSE = RMSE/ (psimax-psimin);
		double[] result = new double[5];
		result[0] = inside;
		result[1] = MSE;
		result[2] = MAE;
		result[3] = RMSE;
		result[4] = NRMSE;
		return result;
	}

	public String GroupsReport(){
		String result = "";
		for (int i = 0; i < AcceptedGroups.size(); i++){
			WekaDataSet theGroup = AcceptedGroups.get(i);
			result += "Group "+i+" data \n";
			result += theGroup.DataSet.toString() + "\n";
			result += "Group "+i+" linear regression model: \n";
			result += theGroup.LinealModel.LinearRegresionModel.toString() + "\n";
                        result += "Group "+i+" evaluateModel: \n";
			result += theGroup.LinealModel.TestOnTraining.toSummaryString() + "\n";
		}
		return result;
	}
        
    public void saveGroups(){
		
		for (int i = 0; i < AcceptedGroups.size(); i++){
            String result = "";
			WekaDataSet theGroup = AcceptedGroups.get(i);
			result += "datos/tuning/final/Groups/Group"+i+".arff"; 
			try {
				DataSink.write(result,theGroup.DataSet);
			}
			catch (Exception e) {
				System.err.println("Failed to save data to: " + result);
				e.printStackTrace();
			}
		}	
	}
	public void clear(){
        this.AcceptedGroups.clear();
    }
	public String Report(){
		try{
			double avgCC = 0.0;
			int sumRows = 0;
			String rowsByGroup = "";
                        DecimalFormat df = new DecimalFormat("###.###");
                        
			for (int i = 0; i < AcceptedGroups.size(); i++){
				double CC = (AcceptedGroups.get(i)).LinealModel.TestOnTraining.correlationCoefficient();
                                int rowsGroup = (AcceptedGroups.get(i)).DataSet.numInstances();
				avgCC += CC * rowsGroup;
				sumRows += rowsGroup;
				rowsByGroup = rowsByGroup.concat(Integer.toString(rowsGroup));
				rowsByGroup = rowsByGroup.concat("(");
                                rowsByGroup = rowsByGroup.concat(df.format(CC));
				rowsByGroup = rowsByGroup.concat("):");
			}
			String result = Integer.toString(AcceptedGroups.size());
                        result = result.concat(" | ");
			result = result.concat(df.format((avgCC/sumRows)));
			result = result.concat(" | ");
			result = result.concat(Integer.toString(sumRows));
                        result = result.concat(" | ");
                        result = result.concat(rowsByGroup);

			return result;
		}
		catch (Exception e){
			throw new RuntimeException(e);
		}
	}
        
	public Distance FinalDistance(ArrayList<Distance> ListDistanceToWeigh,int kn, int cantGr){
		ListDistanceToWeigh.sort(Comparator.comparingDouble(Distance::getDistance));
		ArrayList<Distance> FnListDistanceToWeigh = new ArrayList<Distance>();
		for (int i = 0; i < kn; i++) {
		FnListDistanceToWeigh.add(ListDistanceToWeigh.get(i));
		}
		ArrayList<Distance> FnWeigh = new ArrayList<Distance>();
		for (int i = 0; i < cantGr; i++) {
			double weighGr = 0;
			for (int j = 0; j < FnListDistanceToWeigh.size(); j++) {
				if(FnListDistanceToWeigh.get(j).grupo==i){
					weighGr = weighGr + (1/FnListDistanceToWeigh.get(j).distance);
				}
			}
			FnWeigh.add(new Distance(i,weighGr));
		}
		FnWeigh.sort(Comparator.comparingDouble(Distance::getDistance).reversed());
		return FnWeigh.get(0);
	}
        
	public Instances joinByGroups() throws Exception{
		Instances allgroups=null;
		for (int i = 0; i < AcceptedGroups.size(); i++){
			Add filter;
			Instances clasificationGroups = new Instances(AcceptedGroups.get(i).DataSet);
			// 1. nominal attribute
			filter = new Add();
			filter.setAttributeIndex("last");
			filter.setNominalLabels("0,1,2,3,4,5,6,7,8");
			filter.setAttributeName("group");
			filter.setInputFormat(clasificationGroups);
			clasificationGroups = Filter.useFilter(clasificationGroups, filter);

			for(Instance ins: clasificationGroups){
				ins.setValue(ins.numAttributes()-1,i);
			}
			if(i==0){
				allgroups = new Instances(clasificationGroups);
			}else{
				allgroups.addAll(clasificationGroups);
			}
		}
		return allgroups;
	}
}