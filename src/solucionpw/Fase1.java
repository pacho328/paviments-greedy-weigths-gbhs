/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solucionpw;

import java.util.ArrayList;
import java.util.List;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 *
 * @author Pacho328
 */
public class Fase1 {
    
    public Instances TestData;
    public int MinNumberOfInstancesByGroup;
    public double MaximunAbsoluteErrorPerRow;
    public double qualityDegrade;
    public Instances trainingData;
    public String DataDirectory = "datos/";
    public List<Double> CC = new ArrayList<Double>();
    public double ccrejectedgroups = 0.0;
    public WekaDataSet rejectedDataSet;

    public Fase1(Instances trainingData, double MaximunAbsoluteErrorPerRow, double qualityDegrade,int MinNumberOfInstancesByGroup) throws Exception {
        ConverterUtils.DataSource sourceTest = new ConverterUtils.DataSource(this.DataDirectory.concat("data_test.arff"));
	    this.TestData = sourceTest.getDataSet();

        if (this.TestData.classIndex() == -1)
            this.TestData.setClassIndex(this.TestData.numAttributes() - 1); // psi is the last column
        
        this.trainingData =  new Instances(trainingData);
        this.MaximunAbsoluteErrorPerRow = MaximunAbsoluteErrorPerRow;
        this.qualityDegrade = qualityDegrade;
        this.MinNumberOfInstancesByGroup = MinNumberOfInstancesByGroup;
    }
    
    
    public BestGroup logica(int kn) throws Exception{
        BestGroup bg = new BestGroup();
        while (this.trainingData.numInstances()!=0)
        {
            LinearModelForDataSet lmd = new LinearModelForDataSet(this.trainingData, MaximunAbsoluteErrorPerRow);
            
            if (lmd.BestRows < MinNumberOfInstancesByGroup) {
                if (lmd.DataSet.numInstances() < MinNumberOfInstancesByGroup){
                    lmd.BestRows = lmd.DataSet.numInstances();
                }
                else{
                    lmd.BestRows = MinNumberOfInstancesByGroup;
                }
            }
            WekaDataSet bestRowsInDataSet = new WekaDataSet(lmd.DataSet);
            bestRowsInDataSet.PreserveFirstRows(lmd.BestRows);
            bestRowsInDataSet.DefineLinearModel();
            double ccaceptedgroups = bestRowsInDataSet.LinealModel.TestOnTraining.correlationCoefficient();
            if (ccaceptedgroups > 0.88){
                bg.addWDS(bestRowsInDataSet);
                CC.add(ccaceptedgroups);
            }
            else{
                this.ccrejectedgroups = ccaceptedgroups;
                rejectedDataSet = bestRowsInDataSet;
                break;
            }
            WekaDataSet worstRowsInDataSet = new WekaDataSet(lmd.DataSet);
            worstRowsInDataSet.RemoveFirstRows(lmd.BestRows);
            trainingData = new Instances(worstRowsInDataSet.DataSet);
            MaximunAbsoluteErrorPerRow += qualityDegrade;
        }
        GBHS gbhs = new GBHS(10,10);
        gbhs.initializePopulation(bg,kn);
        Weights weight = gbhs.improvisations(bg, kn);
        gbhs.showBest();
        System.out.println(bg.Report());
        bg.Evaluate(TestData, kn, weight.getValues());
        return bg;
    }

    public Instances getTrainingData() {
        return trainingData;
    }
    
}
