/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solucionpw;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;
import weka.core.Instances;
/**
/**
 *
 * @author Pacho328
 */
public class GBHS {
    public double HMCR = 0.85;
    public double parMin = 0.1;
    public double parMax = 0.4;
    public int iterations;
    public int sizePopulation;
    public long seed = 1;
    Random generator = new Random(this.seed);

    public ArrayList<Weights> Population = new ArrayList<Weights>();

    public GBHS(int SizePopulation,int iterations) {
        this.sizePopulation = SizePopulation;
        this.iterations = iterations;
    }
    
    
    public ArrayList<Weights> initializePopulation(BestGroup bg,int kn) throws Exception{

        for (int j = 0; j < this.sizePopulation; j++) {
            Weights Weight = new Weights(generator);
            double[] weightsHarmony = Weight.getValuesRandom().clone();
//            System.out.println("Creando al individuo "+ (j+1)+".");
            double results[] = bg.Evaluate(kn,weightsHarmony);
            Weight.setPercentage15(results[0]); 
            Weight.setMSE(results[1]); 
            Weight.setMAE(results[2]); 
            Weight.setRMSE(results[3]); 
            Weight.setNRMSE(results[4]); 

            Population.add(Weight);
        }
        this.sort();
        return this.Population;
    }
    
    public boolean isHarmonyPopulation(double[] weightsHarmony){
        boolean status = false;
        int tam = weightsHarmony.length;
        int cont =0;
        if( this.Population.size()!=0) {
            for (int i = 0; i < this.Population.size(); i++) {
                Weights wTemp = this.Population.get(i);
                double[] weightsTem = wTemp.getValues();
                for (int j = 0; j < weightsTem.length; j++) {
                    if(weightsTem[j]==weightsHarmony[j]) cont++;
                }
            }
            if(tam ==cont) status = true;
        }
    return status;
    }
    
    public void sort(){
        this.Population.sort(Comparator.comparingDouble(Weights::getPercentage15).reversed());
    }
    public void showPopulation(){
        for (int i = 0; i < Population.size(); i++) {
            Population.get(i).showAll();
        }
    }
    
    public Weights improvisations(BestGroup bg,int kn) throws Exception{
        double par;

        for (int i = 0; i < this.iterations; i++) {
            Weights Weight = new Weights(generator);
//            System.out.println("iteracion: " +(i));
            par = this.parMin + ((this.parMax-this.parMin) * (i/this.iterations));
            double suma = 0.0;
            for (int j = 0; j < this.Population.get(0).getValues().length; j++) {
                if(generator.nextDouble() < this.HMCR){
                    double[] wPop = this.Population.get(generator.nextInt(this.Population.size())).getValues();
                    Weight.setValue(j, wPop[j]);
                    if(generator.nextDouble()<par){
                        wPop = this.Population.get(0).getValues();
                        Weight.setValue(j, wPop[j]);
                    }
                }else{
                    Weight.getRandomValue(j);
                }
                suma += Weight.getValue(j);
            }
            for (int j = 0; j < this.Population.get(0).getValues().length; j++) {
                Weight.setValue(j,Weight.getValue(j)/suma);
            }
//            System.out.println("iteracion: " +(i+1));
            double results[] = bg.Evaluate(kn,Weight.getValues());
            Weight.setPercentage15(results[0]); 
            Weight.setMSE(results[1]); 
            Weight.setMAE(results[2]); 
            Weight.setRMSE(results[3]); 
            Weight.setNRMSE(results[4]);
            this.Population.add(Weight);
            this.sort();
            this.Population.remove(this.sizePopulation);
        }
        return this.Population.get(0);
    }    
    public void showBest(){
        this.Population.get(0).showAll();
    }
}
