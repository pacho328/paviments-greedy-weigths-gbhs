package solucionpw;

import java.io.File;
import java.io.FileWriter;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;  

public class Interfaz {
    public static void main(String[] args) throws Exception{
	try{
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");  
            LocalDateTime now = LocalDateTime.now();  
            System.out.println(dtf.format(now));  
            int MinNumberOfInstancesByGroup = 1600;
            int afinamiento = 1;

            for (int kn = 1; kn <= 1; kn++) {
                System.out.println("K= "+ kn);
                for (double maepr = 0.025; maepr <=0.025; maepr+=0.005){
                    for (double qD = 0.045; qD <=0.045; qD+=0.005){
                        for (int MaxNumberOfInstancesByGroup = 3000; MaxNumberOfInstancesByGroup < 4000; MaxNumberOfInstancesByGroup = MaxNumberOfInstancesByGroup+1001) {
                            long inicio = System.currentTimeMillis();
                            double MaximunAbsoluteErrorPerRow = maepr;
                            double qualityDegrade = qD;
                                    // Read the data file and define the index for class attribute                
                            Instances trainingData = initDataTraining("data_training.arff");    
                            Fase1 f1 = new Fase1(trainingData,MaximunAbsoluteErrorPerRow,qualityDegrade,MinNumberOfInstancesByGroup);
                            BestGroup bg = f1.logica(kn);

                            trainingData =f1.getTrainingData();
                            int tamañoHuerfanos = trainingData.numInstances();
                            while(tamañoHuerfanos>0){
                                Fase2 f2 = new Fase2(trainingData,bg,afinamiento,inicio,maepr,qD);
                                bg = f2.logica(false,kn);
                                BestGroup finalbg = new BestGroup();
                                Instances TodosLosHuerfanos=null;
                                finalbg.clear();
                                if(tamañoHuerfanos<MinNumberOfInstancesByGroup){   
                                    break;
                                }
                                for (int i = 0; i < bg.AcceptedGroups.size(); i++) {
                                    if(bg.AcceptedGroups.get(i).DataSet.numInstances()<MaxNumberOfInstancesByGroup){
                                        finalbg.AcceptedGroups.add(bg.AcceptedGroups.get(i));
                                        continue;
                                    }
                                    Instances grupTemp = new Instances(bg.AcceptedGroups.get(i).DataSet);

                                    Fase1 temf1 = new Fase1(grupTemp,MaximunAbsoluteErrorPerRow,qualityDegrade,MinNumberOfInstancesByGroup);
                                    BestGroup tembg = temf1.logica(kn);
                                    Instances InstaciasHuerfanas = new Instances(temf1.getTrainingData()); // Nuevos huerfanos
                                    if(TodosLosHuerfanos==null){
                                        TodosLosHuerfanos = new Instances(InstaciasHuerfanas);
                                    }else{
                                        TodosLosHuerfanos.addAll(InstaciasHuerfanas); 
                                    }
                                    double contParcial = 0;
                                    for (int j = 0; j < tembg.AcceptedGroups.size(); j++) {
                                        contParcial = contParcial +tembg.AcceptedGroups.get(j).DataSet.numInstances();
                                        finalbg.AcceptedGroups.add(tembg.AcceptedGroups.get(j));
                                    }   
                                }
                                bg.clear();
                                for (int i = 0; i < finalbg.AcceptedGroups.size(); i++) {
                                    bg.AcceptedGroups.add(finalbg.AcceptedGroups.get(i));
                                }
                                trainingData = TodosLosHuerfanos;
                                if(trainingData==null){
                                    tamañoHuerfanos = 0;
                                }else{
                                    tamañoHuerfanos = trainingData.numInstances();
                                }

                            }
                            afinamiento++;
                            
                            String pathDir =  "datos/tuning/final";
                            File directory = new File(pathDir);
                            directory.mkdir();					
                            File file = new File(pathDir + "/ResultadosFinales.txt");
                            FileWriter fr = new FileWriter(file);
                            fr.write(bg.TablaResultados);
                            fr.close();
                            bg.saveGroups();
                                    
                        }
                    }
                }
            }    
            System.out.println("Fin");
            LocalDateTime now2 = LocalDateTime.now();  
            System.out.println(dtf.format(now2)); 
	}
        catch (Exception e){
        	throw new RuntimeException(e);
        }
    }
    public static Instances initDataTraining(String namefile) throws Exception{
        ConverterUtils.DataSource source = new ConverterUtils.DataSource("datos/"+namefile);
                    Instances trainingData = source.getDataSet();
                    if (trainingData.classIndex() == -1)
                        trainingData.setClassIndex(trainingData.numAttributes() - 1); // psi is the last column
        return  trainingData;      
    }
}