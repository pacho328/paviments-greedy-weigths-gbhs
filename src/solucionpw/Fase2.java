/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solucionpw;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 *
 * @author Pacho328
 */
public class Fase2 {

    public Instances trainingData;
    public Instances TestData;
    public BestGroup bg;
    public int afinamiento;
    public long inicio;
    public String DataDirectory = "datos/";
    public double maepr;
    public double qD;
    public double inside15final;
    public List<Double> CC2 = new ArrayList<Double>();

    public Fase2(Instances trainingData, BestGroup bg, int afinamiento, long inicio, double maepr, double qD) throws Exception {
        ConverterUtils.DataSource sourceTest = new ConverterUtils.DataSource(this.DataDirectory.concat("data_test.arff"));
	    this.TestData = sourceTest.getDataSet();
        if (this.TestData.classIndex() == -1)
            this.TestData.setClassIndex(this.TestData.numAttributes() - 1); // psi is the last column
                        
        this.trainingData = trainingData;
        this.bg = bg;
        this.afinamiento = afinamiento;
        this.inicio = inicio;
        this.maepr = maepr;
        this.qD = qD;
    }
        
        
    public BestGroup logica(boolean flag, int kn) throws IOException, Exception{
        if(bg.AcceptedGroups.size()>0){
            String resItermedio = bg.Report();

            int numInstances = trainingData.numInstances();
            for (int i = 0; i < numInstances; i++ ){
                    Instance theInstance = trainingData.get(i);
                    bg.DefineGroup(theInstance);
                    //if (i==500) break;
            }

            long fin = System.currentTimeMillis();
            double tiempo = (double) ((fin - inicio)/1000);
            GBHS gbhs = new GBHS(10,10);
            gbhs.initializePopulation(bg,kn);
            Weights weight = gbhs.improvisations(bg, kn);
            gbhs.showBest();
            System.out.print(resItermedio);
            System.out.println(bg.Report());
            bg.Evaluate(TestData, kn, weight.getValues());
            // Save models for each found solution ... directory tuning in datos
            if(flag){
                String pathDir = DataDirectory + "/tuning/" + Round(maepr, 6) + "-" + Round(qD, 6);
                File directory = new File(pathDir);
                directory.mkdir();					
                File file = new File(pathDir + "/Resultados.txt");
                FileWriter fr = new FileWriter(file);
                fr.write(bg.TablaResultados);
                fr.write(bg.GroupsReport());
                fr.close();
            }
        }else{
            System.out.println("No se han generado grupos");
            System.out.println("huerfanos: " +trainingData.numInstances());
        }  
        for (int i = 0; i < bg.AcceptedGroups.size(); i++) {
            this.CC2.add((bg.AcceptedGroups.get(i)).LinealModel.TestOnTraining.correlationCoefficient());
        }
        return bg;
    }
    
    public static String Round(double num, int places){
        BigDecimal bd = new BigDecimal(num);
        bd = bd.setScale(places, RoundingMode.HALF_UP);	// 3 decimals
        return Double.toString(bd.doubleValue());
    }
    
}
