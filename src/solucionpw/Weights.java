/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solucionpw;

import java.util.Random;

/**
 *
 * @author Pacho328
 */
public class Weights {
    public double[] allWeights = new double[14];
    public double percentage15;
    public double MSE;
    public double MAE;
    public double RMSE;
    public double NRMSE;
    public Random rd; 
    public double sum = 0;

    public Weights(Random rd) {
        this.rd = rd; 
        this.percentage15 = 0;
    }

    public void setPercentage15(double percentage15) {
        this.percentage15 = percentage15;
    }
    
    public void setMSE(double MSE) {
        this.MSE = MSE;
    }

    public void setMAE(double MAE) {
        this.MAE = MAE;
    }

    public void setRMSE(double RMSE) {
        this.RMSE = RMSE;
    }

    public void setNRMSE(double NRMSE) {
        this.NRMSE = NRMSE;
    }

    public double getPercentage15() {
        return percentage15;
    }

    public void getRandomValue(int i){
        double newWeight = (1.0/7.0)*this.rd.nextDouble();
        this.setValue(i, newWeight);
    }
    public double getValue(int i){
        return this.allWeights[i];
    }
    public double[] setValue(int i, double value){     
        this.allWeights[i] = value;
        return this.allWeights;
    }
    
    public double[] getValues(){
        return this.allWeights;
    }
    
    public double[] getValuesRandom(){
        for (int i = 0; i < this.allWeights.length; i++) {
            this.allWeights[i] = this.rd.nextDouble(); 
            sum +=  this.allWeights[i]; 
        }
        for (int i = 0; i < this.allWeights.length; i++) {
            this.allWeights[i] = this.allWeights[i]/sum; 
        }
    return this.allWeights;
    }
    public void showAll(){
        System.out.print("15: "+this.percentage15+" | "+"MSE: "+this.MSE+" | "+"MAE: "+this.MAE+" | "+"RMSE: "+this.RMSE+" | "+"NRMSE: "+this.NRMSE+" | ");
    }
    
}
