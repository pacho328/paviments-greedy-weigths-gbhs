package solucionpw;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

public class WekaDataSet {
        public double minvalues[]={0,0,0,750,1.55,20,46,11,0,0,0};
        public double maxvalues[]={0,132000,15557,8750,35.15,56,89,81,230,0.63,0};

	public Instances DataSet;
	public LinearRegressionModel LinealModel;
	
	public WekaDataSet(Instances origin){
		DataSet = new Instances(origin);
	}
	
	public void PreserveFirstRows (int numberOfRows){
		for (int i = DataSet.numInstances() - 1; i >= 0; i--) {
			if (i >= numberOfRows) {
				DataSet.delete(i);
			}
		}
	}

	public void RemoveFirstRows (int numberOfRows){
            for (int i = 0; i < numberOfRows; i++) {
                DataSet.delete(0);     
            }
	}
	
	public void DefineLinearModel()
	{
		LinealModel = new LinearRegressionModel(DataSet);
	}
	
	public double EvaluateIncludeInstance(Instance theInstance){
		try{
			Instances copyData = new Instances(DataSet);
			double currentCC = LinealModel.TestOnTraining.correlationCoefficient();
			copyData.add(theInstance);
			LinearRegressionModel theLinealRegressionModel = new LinearRegressionModel(copyData);
			double newCC = theLinealRegressionModel.TestOnTraining.correlationCoefficient();
			return newCC - currentCC;
		}
		catch (Exception e){
			throw new RuntimeException(e);
		}
	}
	
	public void IncludeInstance(Instance theInstance){
		DataSet.add(theInstance);
		LinealModel = new LinearRegressionModel(DataSet);
	}
	
	public double distanceNor(Instance instance1, Instance instance2) {
		double dist = 0.0;

		for (int i = 0; i < instance1.numAttributes()-1; i++) {
                if(instance1.attribute(i).type()==Attribute.NUMERIC){
                    double x = instance1.value(i);
                    double y = instance2.value(i);
                    x = (x - minvalues[i])/(maxvalues[i]-minvalues[i]);
                    y = (y - minvalues[i])/(maxvalues[i]-minvalues[i]); 

                    if (Double.isNaN(x) || Double.isNaN(y)) {
                        continue; // Mark missing attributes ('?') as NaN.
                    }
                    dist += (x-y)*(x-y); 
                }else{
                    if(instance1.value(i)!=instance2.value(i)){
                        dist += 1;
                    }
                }        
		}
		return Math.sqrt(dist);
	}
        
    public double distance(Instance instance1, Instance instance2) {
		double dist = 0.0;

	for (int i = 0; i < instance1.numAttributes()-1; i++) {
		double x = instance1.value(i);
		double y = instance2.value(i);
		if (Double.isNaN(x) || Double.isNaN(y)) {
		    continue; // Mark missing attributes ('?') as NaN.
		}
		dist += (x-y)*(x-y);
	}
	return Math.sqrt(dist);
	}
    public double distanceWeights(Instance instance1, Instance instance2, double[] weightsHarmony) {
        double dist = 0.0;
		for (int i = 0; i < instance1.numAttributes()-1; i++) {
            if(instance1.attribute(i).type()==Attribute.NUMERIC){
                double x = instance1.value(i);
                double y = instance2.value(i);
                if (Double.isNaN(x) || Double.isNaN(y)) {
                    continue; // Mark missing attributes ('?') as NaN.
                }
                dist += weightsHarmony[i]*((x-y)*(x-y)); 
            }else{
				if(instance1.value(i)!=instance2.value(i)){
                    dist += weightsHarmony[i];
                }
            }        
		}
		return Math.sqrt(dist);
	}
	
	public double MinDistanceToTestInstance(Instance theInstance){
		try{
			double minDistance = Double.MAX_VALUE;
			int numInstances = DataSet.numInstances();
			for (int i = 0; i < numInstances; i++){
				Instance rowInstance = DataSet.get(i);
				double dis = distance(theInstance, rowInstance);
				if (dis < minDistance){
					minDistance  = dis;
				}
			}
			return minDistance;
		}
		catch (Exception e){
			throw new RuntimeException(e);
		}
	}
    public ArrayList<Distance> KlistMinDistanceToTestInstance(Instance theInstance, int kn, int ngro,double[] weightsHarmony){
		try{     
			ArrayList<Distance> minDistance = new ArrayList<Distance>();
                        
			int numInstances = DataSet.numInstances();
			for (int i = 0; i < numInstances; i++){
                            Instance rowInstance = DataSet.get(i);
                            double dis = distanceWeights(theInstance, rowInstance,weightsHarmony);
                            if (minDistance.size()<kn){
                                minDistance.add(new Distance(ngro,dis));
                                minDistance.sort(Comparator.comparingDouble(Distance::getDistance));
                            }else{
                                if(dis<minDistance.get(minDistance.size()-1).distance){
                                    minDistance.set(minDistance.size()-1,new Distance(ngro,dis));
                                    minDistance.sort(Comparator.comparingDouble(Distance::getDistance));
                                }
                            }	
                        }
			
			return minDistance;
		}
		catch (Exception e){
			throw new RuntimeException(e);
		}
	}
	
	public double PredictPSI(Instance theInstance){
		try {
			double[] predictedPSI = LinealModel.LinearRegresionModel.distributionForInstance(theInstance);
			return predictedPSI[0];
		}
		catch (Exception e){
			throw new RuntimeException(e);
		}
	}
}