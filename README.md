# paviments-greedy-weigths-gbhs

Este código corresponde a un artículo científico escrito por MG(c). [Francisco Anacona](http://artemisa.unicauca.edu.co/~javieranacona/ "Página Universidad del Cauca"), Phd. [Carlos Cobos](http://artemisa.unicauca.edu.co/~ccobos/ "Página Universidad del Cauca"), Phd. [Martha Mendoza](http://artemisa.unicauca.edu.co/~mmendoza/ "Página Universidad del Cauca") y Phd. [Enrrique Herrera](https://ccia.ugr.es/~viedma/ "página Universidad granada").

## Requisitos:

[JDK 8](https://www.oracle.com/es/java/technologies/javase/javase8-archive-downloads.html)

[Netbeans 8](https://netbeans.apache.org/download/index.html)

[Git](https://git-scm.com/downloads)
## Pasos:
Paso 1:

>`git clone https://gitlab.com/pacho328/paviments-greedy-weigths-gbhs`

Paso 2: 

En Netbeans ir a la opción de Archivo.

![Paso1](https://i.ibb.co/QDcn4Rx/Captura-de-Pantalla-115.png)

Paso 3: 

Ir al abrir proyecto

![Paso2](https://i.ibb.co/BGqqXpz/Captura-de-Pantalla-116.png)

Paso 4:

Seleccionar la ubicación donde se clonó el repositorio.

![Paso3](https://i.ibb.co/V2B2T7D/Captura-de-Pantalla-117.png)

Paso 5:

El programa detecta que faltan unas librerías dar click en resolver problema.

![Paso4](https://i.ibb.co/4tLSNhK/Captura-de-Pantalla-118.png)

Paso 6:

Por cada librería faltante se tiene que dar click en resolver.

![Paso5](https://i.ibb.co/JBnK9tm/Captura-de-Pantalla-119.png)

Paso 7:

En el repositorio existe una carpeta llamada **lib** se debe seleccionar la librería solicitada

![Paso6](https://i.ibb.co/HPjG4jV/Captura-de-Pantalla-120.png)

Paso 8: 

Validar que todas las librerías estén cargas.

![Paso7](https://i.ibb.co/VMZ4B5Y/Captura-de-Pantalla-121.png)

Paso 9: 

Dar click en el botón de correr o ejecutar

![Paso8](https://i.ibb.co/yyfR35n/Captura-de-Pantalla-122.png)